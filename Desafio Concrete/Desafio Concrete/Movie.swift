//
//  Movie.swift
//  Desafio Concrete
//
//  Created by vinicius.tenorio on 7/18/18.
//  Copyright © 2018 Vinicius Tenorio. All rights reserved.
//

import Foundation

struct MovieResult: Decodable {
    let results: [Movie]?
}

struct Movie : Codable {
    let vote_count : Int?
    let id : Int?
    let video : Bool?
    let vote_average : Double?
    let tittle : String?
    let popularity : Double?
    let poster_path : String?
    let original_language: String?
    let original_title :String?
    let backdrop_path: String?
    let adult: Bool?
    let overview: String?
    let release_date: String?
    
}



