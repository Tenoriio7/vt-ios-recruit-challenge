//
//  File.swift
//  Desafio Concrete
//
//  Created by vinicius.tenorio on 7/18/18.
//  Copyright © 2018 Vinicius Tenorio. All rights reserved.
//

import Foundation

class REST  {
    private static let basePath =
    "https://api.themoviedb.org/3/movie/popular?api_key=fb5e33646096bb28bda1c0c6d52895d4&language=en-US&page=1"
    private  static let  configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["Content-Type":"application/json"]
        return config
    }()
    private  static let session = URLSession(configuration: configuration)
    
    class func loadMovies(){
        guard let url = URL(string: basePath) else {return}
        let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            guard let response = response as? HTTPURLResponse else {return}
            print(response.statusCode)
            if  response.statusCode == 200 {
                guard let data = data else {return}
                do {
                    let moviesResult = try JSONDecoder().decode(MovieResult.self, from: data)
                    guard  let movies = moviesResult.results else {return}
                    for movie in movies {
                        print (movie.original_title)
                    }
                } catch{
                    print(error.localizedDescription)
                }
                
            }
        }
        dataTask.resume()
    }
    
}
